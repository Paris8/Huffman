#ifndef _ARBRE_HUFFMAN_H_
#define _ARBRE_HUFFMAN_H_ 1

#include <stdio.h>
#include <stdlib.h>

#include "liste.h"

// Définition de l'Arbre.
typedef Cellule *Arbre;

// Définition d'une lettre/caractère pour l'entete
typedef struct entete {
    char lettre; // la lettre / caractère
    int code; // code binaire
    int longueur; // longueur du code
} Entete;

// Convertis une liste (struct Liste) en arbre (struct Arbre) et renvoie la taille des caractères valides de la liste.
int listeVersArbre(Liste *liste);

// Encode un fichier.
void compression(FILE *entree, FILE *sortie);

// Décode un fichier.
void decompression(FILE *entree, FILE *sortie);

/*
    Ajoute les valeurs des lettres dans l'arbre récursivement.
        - `arbre` l'arbre binaire de huffman
        - `codeActuel` monte progresssivement quand on parcours l'arbre et c'est la valeur assigné aux noeuds
        - `longueur` la profondeur du noeud dans l'arbre
        - `lettresListe` notre liste de lettre
        - `i` nous permet de se balader dans la liste `lettresListe` (c'est là où l'on se trouve)
        - `longueurTotale` est la longueur totale du code en sortie en bits

    J'essaie de favoriser l'utilisation de pointeur pour éviter de recopier plus fois la même chose en mémoire.
    Parcours prefixé de l'arbre.
*/
void assignationCode(Arbre arbre, int codeActuel, int longueur, Entete *enteteListe, int *i, int *longueurTotale);

// Libère en mémoire un arbre (struct Arbre).
void freeArbre(Arbre arbre);

// Convertis un arbre (struct Arbre/Liste) en liste (struct Entete).
Entete *arbreVersListe(Arbre arbre, int taille, int *tailleTotale);

// Lie le fichier d'entrée pour le transformer en liste (struct Entete) de caractères.
Entete *fichierVersListe(FILE *fichier, int *nombreLettresDansFichier, int *tailleTotale);

// Écrit l'entête avec la table de Huffman dans le fichier.
void enteteVersFichier(Entete *enteteListe, int nombreLettresDansFichier, int longueurTotale, FILE *fichier);

// Écrit les données une fois traduite après l'algorithme de Huffman dans le fichier de sortie.
void huffmanVersFichier(FILE *entree, FILE *sortie, Entete *enteteListe, int nombreLettresDansFichier);

// Récupère les infos d'une lettre depuis l'entête.
Entete recuperationLettre(char lettre, Entete *enteteListe, int nombreLettresDansFichier);

/*
    Lis les données d'un fichier .huff et le transforme en Arbre binaire Huffman (struct Arbre).
    Définit aussi la taille du fichier.
*/
Arbre lectureDonnees(FILE *fichier, int *tailleTotale);

// Retranscris l'arbre de Huffman en fichier lisible.
void huffmanDepuisFichier(FILE *entree, FILE *sortie, Arbre arbre, int tailleTotale);

#endif
