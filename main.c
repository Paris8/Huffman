#include <string.h>

#include "liste.h"
#include "arbre.h"

// Affiche le message d'aide.
void help(char const progName[]);

// Change l'extension d'un fichier.
char *fichierExtension(const char *fichier, char *extension);

int main(int argc, char const *argv[]) {
 /* Morceau de code qui m'a permis de testé `liste.c`

    Liste l = NULL;
    FILE *f;

    if ((f = fopen("res_fprintf.txt", "w")) == NULL) {
       printf("Erreur lors de l'ouverture du fichier.\n");
       return 1;
    }

    ajouterLettre(&l, 'a');
    ajouterLettre(&l, 'c');
    ajouterLettre(&l, 'c');
    ajouterLettre(&l, 'b');
    ajouterLettre(&l, 'b');
    ajouterLettre(&l, 'b');
    ajouterLettre(&l, 'b');
    ajouterLettre(&l, 'c');
    ajouterLettre(&l, 'e');
    trierListe(&l);

    Cellule *c = allouerCellule('x');
    c->frequence=2;
    ajouterRangee(&l, c);


    ajouterLettre(&l, 'r');

    Cellule *d = allouerCellule('w');
    ajouterRangee(&l, d);

    afficherListe(l, f);

    fclose(f);

    viderListe(l);
 */

    if (argc < 2) { // si aucun argument
        printf("Aucun argument n'a été renseigné.\n");
        help(argv[0]); // affichage du message d'aide
        return 1;
    }

    if (argc == 2) { // si un seul argument de renseigné
        if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) { // affiche le message d'aide
            help(argv[0]);
            return 2;
        }
        FILE *entree;
        if ((entree = fopen(argv[1], "r")) == NULL) { // ouverture du fichier + gestion erreur
            printf("Erreur lors de l'ouverture du fichier.\n");
            return 1;
        }

        printf("Compression de %s...\n", argv[1]);

        char *nomFichierCompressee = fichierExtension(argv[1], ".huff");
        FILE *sortie = fopen(nomFichierCompressee, "wb");
        compression(entree, sortie); // compression du fichier

        fclose(entree); // fermeture du fichier de base
        fclose(sortie); // fermeture du nouveau fichier compressé
        free(nomFichierCompressee); // free du nom car on a utilisé malloc dans `fichierExtension`
        return 0;
    }

    if (argc > 3) { // s'il y a plus de 3 arguments
        printf("Trop d'arguments renseignés.\n");
        help(argv[0]); // affichage du message d'aide
        return 1;
    }

    if (strcmp(argv[1], "-d") == 0 && argc == 3) { // s'il y a 3 arguments et que l'option `d` est renseigné
        FILE *entree;
        if ((entree = fopen(argv[2], "rb")) == NULL) { // ouverture du fichier + gestion erreur
            printf("Erreur lors de l'ouverture du fichier.\n");
            return 1;
        }

        printf("Décompression de %s...\n", argv[2]);

        char *nomFichierDecompressee = fichierExtension(argv[2], ".txt");
        FILE *sortie = fopen(nomFichierDecompressee, "w");
        decompression(entree, sortie); // décompression du fichier

        fclose(entree); // fermeture du fichier de base
        fclose(sortie); // fermeture du nouveau fichier compressé
        free(nomFichierDecompressee); // free du nom car on a utilisé malloc dans `fichierExtension`
        return 0;
    }

    // Si quelque chose ne s'est pas passé comment prévu
    printf("Arguments incorrect, réessayez.\n");
    help(argv[0]); // affichage du message d'aide
    return 1;
}

void help(char const progName[]) {
    printf("=================================== HUFFMAN ===================================\n");
    printf("-> %s <fichier>         || Construit le fichier <fichier>.huff compressé.\n", progName);
    printf("-> %s -d <fichier>.huff || Décompresse le fichier préalabrement compressé.\n", progName);
    printf("-> %s -h, --help        || Affiche ce message d'aide.\n", progName);
    printf("===============================================================================\n");
}

char *fichierExtension(const char *fichier, char *extension) {
    /*
        Récupération informations concernant la position
        du dernier point et de la taille du nom du fichier.
    */
    int positionDernierPoint = -1;
    int tailleNomFichier = 0;
    while (fichier[tailleNomFichier] != '\0') {
        tailleNomFichier++;
        if (fichier[tailleNomFichier] == '.') positionDernierPoint = tailleNomFichier;
    }

    char *resultat;
    if (positionDernierPoint != -1 && tailleNomFichier - positionDernierPoint <= 5) { // s'il y a une extension à la fin du fichier (c-à-d 4 lettres max après le dernier point dans le nom du fichier [on compte le point])
        int tailleNouveauFichier = tailleNomFichier - (tailleNomFichier - positionDernierPoint); // taille fichier sans extension

        char *fichierSansExtension;
        if ((fichierSansExtension = malloc(tailleNouveauFichier * sizeof(char))) == NULL) { // variable du fichier sans son extension, exemple : `text.txt` devient `text` + gestion erreur
            printf("Erreur lors du malloc qui alloue de l'espace pour le nom du fichier avec une nouvelle extension (1).\n");
            exit(1);
        }
        for (int i = 0; i < tailleNouveauFichier; i++)
            fichierSansExtension[i] = fichier[i];

        if((resultat = malloc((tailleNouveauFichier + strlen(extension)) * sizeof(char))) != NULL) { // on ajoute la taille de l'extension car on va la rajouter au str
            resultat[0] = '\0'; // pour être sur que le string est vide
        } else {
            printf("Erreur lors du malloc qui alloue de l'espace pour le nom du fichier avec une nouvelle extension (2-1).\n");
            exit(1);
        }
        strcat(resultat, fichierSansExtension); // écrit le nom de l'ancien fichier sans son extension

        free(fichierSansExtension); // on free le malloc que l'on vient de faire
    } else {
        if((resultat = malloc((tailleNomFichier + strlen(extension)) * sizeof(char))) != NULL) { // on ajoute la taille de l'extension car on va la rajouter au str
            resultat[0] = '\0'; // pour être sur que le string est vide
        } else {
            printf("Erreur lors du malloc qui alloue de l'espace pour le nom du fichier avec une nouvelle extension (2-2).\n");
            exit(1);
        }
        strcat(resultat, fichier); // écrit le nom de l'ancien fichier complètement
    }

    strcat(resultat, extension); // ajoute l'extension

    return resultat;
}
