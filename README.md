# [Compresseur - Décompresseur (Huffman)](https://defelice.up8.site/tdtpstruct/projet_huffv1.pdf)

|                     |                                          |
|--------------------:|------------------------------------------|
|                 Nom | Anri KENNEL                              |
|              Classe | L2-A                                     |
|   Numéro d'étudiant | 20010664                                 |
|                Mail | anri.kennel@etud.univ-paris8.fr          |
| Cycle universitaire | 2021-2022                                |
|              Module | Algorithmique et structures de données 1 |

## But
Programme qui compresse et décompresse des fichiers selon le codage de Huffman (compression/décompression sans perte de données).

Code utilisé pour la compression inclus dans le fichier compressé.

## Utilisation
### Compilation
Pour compiler le programme, utilisez `make`.

### Lancement
- `./huff <fichier>` construit un fichier `<fichier>.huff` compressé selon le code de Huffman
- `./huff -d <fichier>.huff` qui décompresse le fichier préalabrement compressé par le programme

## Améliorations
- [x] Options supplémentaires (message d'aide `--help` ou `-h`)
- [ ] Taille du code de Huffman réduite dans le fichier (la table est intégrée au fichier ?)
- [ ] Construction à la volée du code de Huffman (?)

### Tags
- Liste chaînée
- Construction et parcours d’arbres
- Lecture et écriture dans des fichiers
- Manipulation de bits

---
### Sources et infos
- [Explication graphique de comment Huffman fonctionne](http://lwh.free.fr/pages/algo/compression/huffman.html)
- [Explication sur comment on fait des opérations sur des bits](https://stackoverflow.com/a/10493604) et [ces exemples](https://www.tutorialspoint.com/cprogramming/c_bitwise_operators.htm)
- Testé avec `gcc (Ubuntu 11.2.0-7ubuntu2) 11.2.0`.
