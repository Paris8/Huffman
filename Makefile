# Nom de l'exécutable
PROG_NAME = huff


# Fichiers que le Makefile va utilisé pour construire l'exécutable
SOURCES = main.c liste.c arbre.c
# Noms des fichiers objets créés lors de la compilation
OBJ = $(SOURCES:.c=.o)


# Compilateur utilisé
CC = gcc
# Options utilisés par le compilateur
CC_FLAGS = -Wall -Wextra -fanalyzer -g -O3


# Permet de supprimer les fichiers objets
CLEAN = rm
# Options utilisés par le programme qui supprime les fichiers
CLEAN_FLAGS = -f


# Créer l'exécutable par défaut
all: $(PROG_NAME)

# Créer les fichiers objets du programme
$(PROG_NAME): $(OBJ)
	$(CC) $^ -o $(PROG_NAME)
	$(CLEAN) $(CLEAN_FLAGS) *.o

# Lie les fichiers objets entre eux pour créer le programme
%.o: %.c
	$(CC) $(CC_FLAGS) -c $<

# Supprime le programme
clean:
	$(CLEAN) *.o $(CLEAN_FLAGS) $(PROG_NAME)
