#include "liste.h"

void ajouterLettre(Liste *liste, char lettre) {
    Cellule *curseur = *liste;
    if (curseur == NULL) { // si liste vide, on créer juste une nouvelle cellule
        *liste = allouerCellule(lettre);
    } else {
        while (curseur->suivant != NULL && curseur->lettre != lettre) // tant qu'on est pas à la fin de la liste ou qu'on n'a pas atteint la lettre
            curseur = curseur->suivant; // on avance dans la liste
        if(curseur->suivant == NULL && curseur->lettre != lettre) // si élément pas dans la liste (on a atteint la fin de la liste et on est pas sur la lettre)
            curseur->suivant = allouerCellule(lettre); // on rajoute la lettre
        else // sinon c'est que on a trouvé la lettre
            curseur->frequence++; // on incrémente de 1 la fréquence de la lettre
    }
}

Cellule *allouerCellule(char lettre) {
    Cellule *nouvelleCellule;
    if ((nouvelleCellule = (Cellule*)malloc(sizeof(Cellule))) == NULL) { // on alloue de la mémoire pour notre nouvelle cellule
       printf("Impossible d'allouer de la mémoire supplémentaire.\n"); // gestion de l'erreur
       exit(1);
    }

    nouvelleCellule->lettre = lettre;
    nouvelleCellule->frequence = 1; // fréquence par défaut
    nouvelleCellule->gauche = NULL;
    nouvelleCellule->droite = NULL;
    nouvelleCellule->suivant = NULL;

    return nouvelleCellule;
}

void afficherListe(Liste liste, FILE *fichier) {
    for (; liste != NULL; liste = (*liste).suivant) // on parcours la liste
        fprintf(fichier, "Lettre `%c` ⋅ %d\n", (*liste).lettre, (*liste).frequence); // on affiche l'élément actuel (lettre + fréquence)
}

void viderListe(Liste liste) {
    Cellule *curseur = liste;
    Cellule *precedent;
    while (curseur != NULL) { // parcours de la liste
        precedent = curseur;
        curseur = curseur->suivant; // on va au suivant
        free(precedent); // on supprime l'ancien
    }
}

void trierListe(Liste *liste) {
    if (*liste != NULL) // si la liste n'est pas vide
        for (Cellule *i = *liste; i->suivant != NULL; i = i->suivant) // premier curseur
            for(Cellule *j = i->suivant; j != NULL; j = j->suivant) // second curseur
                if (i->frequence > j->frequence) { // si besoin de faire un échange, on le fais
                    Cellule temporaire = *j;
                    temporaire.lettre = j->lettre;
                    temporaire.frequence = j->frequence;

                    j->lettre = i->lettre;
                    j->frequence = i->frequence;

                    i->lettre = temporaire.lettre;
                    i->frequence = temporaire.frequence;
                }
}

void ajouterRangee(Liste *liste, Cellule *nouvelleCellule) {
    if (*liste == NULL || (*liste)->frequence >= nouvelleCellule->frequence) { // si liste nulle ou alors tête de liste égal en fréquence à notre nouvelle cellule alors on l'ajoute
        nouvelleCellule->suivant = *liste;
        *liste = nouvelleCellule;
    } else {
        Cellule *curseur;
        curseur = *liste;
        while (curseur->suivant != NULL && curseur->suivant->frequence < nouvelleCellule->frequence) // on remonte la liste vers la queue et on s'arrête soit quand on est arrivé au bout soit quand on a trouvé une place pour mettre la nouvelle cellule
            curseur = curseur->suivant;
        nouvelleCellule->suivant = curseur->suivant;
        curseur->suivant = nouvelleCellule;
    }
}
