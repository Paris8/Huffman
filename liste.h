#ifndef _LISTE_HUFFMAN_H_
#define _LISTE_HUFFMAN_H_ 1

#include <stdio.h>
#include <stdlib.h>

// Définition d'une Celulle/Noeud.
typedef struct cel {
    char lettre; // lettre
    int frequence; // fréquence dans laquelle la lettre apparaît dans le texte
    struct cel *gauche; //  utile en tant que noeud gauche de l'arbre
    struct cel *droite; //  utile en tant que noeud droit de l'arbre
    struct cel *suivant; // utile en tant que cellule de la liste
} Cellule;

// Définition d'une Liste chainée.
typedef Cellule *Liste;

/*
    Ajoute une lettre (qui sera automatiquement transformé
    en cellule si elle n'existe pas déjà) dans la liste (en tête
    de liste), avec sa bonne occurence (la met à jour si la lettre
    existait déjà).
*/
void ajouterLettre(Liste *liste, char lettre);

// Alloue une cellule pour la liste.
Cellule *allouerCellule(char lettre);

// Affiche la liste.
void afficherListe(Liste liste, FILE *fichier);

// Vide la liste.
void viderListe(Liste liste);

// Trie la liste en fonction des occurences (tri à bulle).
void trierListe(Liste *liste);

// Ajoute une cellule dans une liste rangée en préservant l'ordre
void ajouterRangee(Liste *liste, Cellule *cellule);

#endif
